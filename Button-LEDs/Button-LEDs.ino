
int LEDpins[9] = {2,3,4,5,6,7,8,9,10};
int numLEDs = 9;

// Which level have we reached
int numberOfBlinks = 1;

int lightSequence[30];

// Delay between blinks
int blinkDelay = 700;

// The number that should start the game
int startNumber = 1337;

// Are we playing?
bool isPlaying = false;

// How far in the game are we
int gameIndex = 0;

// Are we checking for an answer?
bool isChecking = false;

void setup() {

    Serial.begin(9600);

    for(int i = 0; i < numLEDs; i++) {
        pinMode(LEDpins[i], OUTPUT);
    }
    
}

void loop() {

    // If something is happening, the game shouldn't move on just yet
    if(isChecking) {
        return;
    }

    // if there is something available coming from the serial port
    if (Serial.available()) {

        // Get the incoming number
        int incoming = Serial.parseInt();

        // Decide what to do
        if(incoming == startNumber) {
            if(!isPlaying) {
                // Start the game
                startGame();
                return;
            }
        }

        if(isPlaying) {
            // If incoming seems like an index, it probably is, check if it's correct and continue game
            if(incoming < numLEDs) {
                // Check if correct
                checkAnswer(incoming);
            }
        }

    }
}

void startGame() {
    randomSeed(analogRead(0));
    gameIndex = 0;

    for(int i = 0; i < numberOfBlinks; i++) {
        lightSequence[i] = random(0, numLEDs);
    }

    displaySequence();

}

void displaySequence() {

    for(int i = 0; i < numberOfBlinks; i++) {
        int LED = LEDpins[lightSequence[i]];

        digitalWrite(LED, HIGH);
        delay(blinkDelay);
        digitalWrite(LED, LOW);
        delay(blinkDelay);
    }

    isPlaying = true;

}

// This displays the won animation, and ends the game
void wonGame() {
    // Loop through the LEDs and toggle them
    for (int i = 0; i < numLEDs; i++) {
        digitalWrite(LEDpins[i], HIGH);
        delay(100);
        digitalWrite(LEDpins[i], LOW);
    }

    delay(1000);

    // Reset variables
    gameIndex = 0;
    numberOfBlinks++;
    isPlaying = false;
    isChecking = false;

    // Start the game in a new round
    startGame();
}

void lostGame() {
    wrong();

    Serial.print(0);
    numberOfBlinks = 1;
    isPlaying = false;
}

void checkAnswer(int answer) {
    isChecking = true;

    if(lightSequence[gameIndex] == answer) {
        // Answer is correct, let's send that back
        Serial.print(1);
        correct();

        if(gameIndex+1 == numberOfBlinks) {
            wonGame();
            return;
        }

        gameIndex++;
    } else {
        // The answer is wrong, let's say that and end the game
        lostGame();
    }

    delay(50);
    isChecking = false;
}

void correct() {
    // Blink green LED's for a second
    for (int i = 3; i < 6; ++i) {
        digitalWrite(LEDpins[i], HIGH);
    }
    delay(300);
    for (int i = 3; i < 6; ++i) {
        digitalWrite(LEDpins[i], LOW);
    }
}

void wrong() {

    for (int i = 0; i < 3; ++i) {
        // Blink red LED's for a second
        for (int i = 6; i < 9; ++i) {
            digitalWrite(LEDpins[i], HIGH);
        }
        delay(300);
        for (int i = 6; i < 9; ++i) {
            digitalWrite(LEDpins[i], LOW);
        }
        delay(150);
    }
}
