
// Start button pin
int startPin = 11;

// Pins that are button inputs
int buttonPins[] = {2,3,4,5,6,7,8, 9,10, startPin};

// Number of buttons
int numButtons = 10;

// Array for containing button states
int buttonStates[10];

// The number to send Arduino 2 to start
int startNumber = 1337;

// Is the game playing
bool isStarted = false;

bool isWaitingForAnswer = false;

void setup() {

    Serial.begin(9600);

    for(int i = 0; i < numButtons; i++) {
        pinMode(buttonPins[i], INPUT);
        buttonStates[i] = LOW;
    }

}

void loop() {

    // if there is something available coming from the serial port
    if (Serial.available()) {

        // Get the incoming number
        int incoming = Serial.parseInt();

        if(!isStarted) {
            return;
        }

        if(incoming == 1) {
            // Answer was correct, continue
            isWaitingForAnswer = false;
        } 
        else if(incoming == 0) {
            // Answer was wrong, end game
            isStarted = false;
            isWaitingForAnswer = false;
        }

    }

    for(int i = 0; i < numButtons; i++) {
        if(digitalRead(buttonPins[i]) == HIGH) {

            // Do something is button state was previously LOW
            if(buttonStates[i] == LOW) {
                buttonStates[i] = HIGH;

                // Check if pin is the start button
                if(buttonPins[i] == startPin) {
                    if(!isStarted) {
                        // Start the game
                        startGame();
                        return;
                    }
                }

                if(isStarted && !isWaitingForAnswer) {
                    // Send the guess and wait for answer
                    Serial.print(i);
                    isWaitingForAnswer = true;
                }

            }

        } else {
            buttonStates[i] = LOW;
        }
    }

    delay(50);
}

void startGame() {
    Serial.print(startNumber);
    isStarted = true;
}   

